# T4C KC

| Serveur             | Status                                                                                                                                                           |
| :---                |    :----                                                                                                                                                         |
| Realmud             | [![Realmud](https://gitlab.com/lysakowskimg/t4c_kc/-/jobs/artifacts/master/raw/realmud.svg?job=testing_realmud)](https://gitlab.com/lysakowskimg/t4c_kc)          |
| Abomination         | [![Abomination](https://gitlab.com/lysakowskimg/t4c_kc/-/jobs/artifacts/master/raw/abomination.svg?job=testing_abomination)](https://gitlab.com/lysakowskimg/t4c_kc)  |
| Saga                | [![Saga](https://gitlab.com/lysakowskimg/t4c_kc/-/jobs/artifacts/master/raw/saga.svg?job=testing_saga)](https://gitlab.com/lysakowskimg/t4c_kc)                |
| Neerya              | [![Neerya](https://gitlab.com/lysakowskimg/t4c_kc/-/jobs/artifacts/master/raw/neerya.svg?job=testing_neerya)](https://gitlab.com/lysakowskimg/t4c_kc)            |