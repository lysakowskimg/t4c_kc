import socket
import os

"""
Trame de connexion UDP par le client sur le serveur neerya.t4c.com (107.181.161.20)
59	8.337042	192.168.1.34	107.181.161.20	UDP	48	56345 → 12177 Len=6
0000   d4 60 e3 14 07 80 60 45 cb 63 04 ee 08 00 45 00   .`....`E.c....E.
0010   00 22 d6 16 00 00 80 11 00 00 c0 a8 01 22 6b b5   ."..........."k.
0020   a1 14 dc 19 2f 91 00 0e ce b3 01 00 12 00 01 86   ..../...........
"""

def serverIsUp(host=os.environ.get("SERVER", "neerya.t4c.com"), port=os.environ.get("PORT", 12177), hex_data="01 00 12 00 01 86"):
    """
        Fonction permettant de tester via un socket UDP si le serveur répond où non
        Calque sur le client T4C
        Args:
            * host(str) : Le nom DNS du serveur T4C
            * port(int) : Le port du serveur T4C
            * hex_data(str) : La chaine Hexa au format str
        Return:
            bool : True / False
    """
    # Creation d'un socket UDP
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    # Ajout d'un timeout de 10 secondes
    UDPClientSocket.settimeout(10)
    print("HOST ", host)
    print("PORT ", port)

    # On test
    try:
        # Envoi des datas sur le serveur
        UDPClientSocket.sendto(bytes.fromhex(hex_data), (host, int(port)))
        # On recupere le message
        msg, addr = UDPClientSocket.recvfrom(1024)
    except socket.error as msg:
        # Si aucune données
        print("Error ", msg)
        return False
    except socket.timeout as msg:
        print("Timeout ", msg)
        return False
    else:
        # Si on reçoit des données
        print("Msg ", msg)
        return True

if __name__ == "__main__":
    if serverIsUp() == True:
        exit(0)
    else:
        exit(1)